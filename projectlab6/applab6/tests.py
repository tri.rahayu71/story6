from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import*
from .forms import *

# Create your tests here.

class testing(TestCase):
	def test_lab_5_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200) #kalo client akses url ini, responnya 200

	def test_lab5_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, landingPage)

	def test_model_can_create_new_todo(self):
		# Creating a new activity
		new_status = Status.objects.create(stats="hehe")

		# Retrieving all available activity
		counting_all_available_todo = Status.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)
	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = formStatus()
		self.assertIn('class="todo-form-input', form.as_p())
		# form as p- as paragraph?
		# buat ngecek ada apa ga di webnya
	def test_form_validation_for_blank_items(self):
		form = formStatus(data={'stats': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		form.errors['stats'], ["This field is required."]
		)
	def test_lab5_post_success_and_render_the_result(self):
		test = 'hehe'
		response_post = Client().post('/addStatus', {'stats': test})
		self.assertEqual(response_post.status_code, 302) #masuk database

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)


