from django import forms
from .models import Status



class formStatus(forms.Form):
	title_attrs = {
	    'type': 'text',
	    'class': 'todo-form-input',
	    'placeholder':'Silahkan tulis status anda di sini '
	}
	stats = forms.CharField(label="STATUS", widget=forms.TextInput(attrs=title_attrs))
