from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from .forms import *
# Create your views here.

response={}

def landingPage(request):
	todo = Status.objects.all()
	response['todo'] = todo
	
	response['formStatus'] = formStatus()
	return render(request, 'landingpage.html', response)

def addStatus(request):
	form = formStatus(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['stats'] = request.POST['stats']
		todo = Status(stats=response['stats'])
		todo.save()
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')
