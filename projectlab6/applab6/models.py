from django.db import models

# Create your models here.

class Status(models.Model):
    stats = models.CharField(max_length=1000000)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)